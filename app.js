
const comboBox = document.querySelector('#razas');
const img = document.querySelector('#imgRaza');
const imgContainer = document.querySelector('.img-container');
const error = document.querySelector('#error');

document.addEventListener('DOMContentLoaded', () => {
    const options = [];

    const url = `https://dog.ceo/api/breeds/list`;
    axios.get(url)
    .then( (response) => {
        const razas = response.data.message;
        razas.forEach( (raza) => {
            options.push(`<option value="${raza}">${raza}</option>`);
        });
        comboBox.innerHTML = options.join('');
    })
    .catch( (error) => {
        comboBox.innerHTML = '<option value="">No se pudo cargar la lista de razas</option>';
        img.src = '';
        console.error('Error al realizar la solicitud:', error);
    });
});

function buscarRaza(){
    const raza = comboBox.value;
    if(raza){
        const url = `https://dog.ceo/api/breed/${raza}/images/random`;
        axios.get(url)
            .then(response => {
                img.src = response.data.message;
                error.innerHTML = '';
            })
            .catch(error => {
                error.innerHTML = 'No se pudo cargar la imagen';
                img.src = '';
                console.error('Error al realizar la solicitud:', error);
            });
    }
}

document.getElementById('btnCargar').addEventListener('click', buscarRaza);